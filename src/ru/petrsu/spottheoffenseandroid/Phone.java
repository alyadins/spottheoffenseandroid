package ru.petrsu.spottheoffenseandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import ru.petrsu.spottheoffenseandroid.network.RequestGenerator;
import ru.petrsu.spottheoffenseandroid.network.Response.PhoneResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lexer on 02.06.14.
 */
public class Phone extends Activity {

    private static final String URL = "http://spottheoffense.tk/api/auth/phone";

    private EditText mPhoneNumber;
    private View mAgreementView;
    private CheckBox mAgreementCheckBox;

    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone);

        mPhoneNumber = (EditText) findViewById(R.id.phone);
        mAgreementView =  findViewById(R.id.agreement);
        mAgreementCheckBox = (CheckBox) findViewById(R.id.agreement_checkbox);
        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.wait_message));

        mAgreementView.setOnClickListener(mOnClickListener);

    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            if (id == mAgreementView.getId()) {
                mAgreementCheckBox.setChecked(!mAgreementCheckBox.isChecked());
            }

        }
    };

    private RequestGenerator.OnRequestCompleteListener onRequestCompleteListener = new RequestGenerator.OnRequestCompleteListener() {
        @Override
        public <T> void onComplete(T response) {
            PhoneResponse r = (PhoneResponse) response;

            dialog.dismiss();
            if (r.code.equals("200")) {
                writeToken(r.token, Phone.this);
                startActivity(new Intent(Phone.this, Pin.class));
                finish();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(Phone.this);
                builder.setCancelable(false)
                        .setMessage(Phone.this.getResources().getString(R.string.phone_error))
                        .setNegativeButton(Phone.this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                builder.create().show();
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.ok_action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.continue_action:
                if (mAgreementCheckBox.isChecked()) {
                    String phone = mPhoneNumber.getText().toString();
                    SharedPreferences preferences = getSharedPreferences(Main.SETTINGS_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Main.PHONE, phone);
                    editor.commit();
                    dialog.show();
                    createRequest(phone, Phone.this, onRequestCompleteListener);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Phone.this);
                    builder.setCancelable(false)
                            .setMessage(Phone.this.getResources().getString(R.string.agreement_error))
                            .setPositiveButton(Phone.this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    builder.create().show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static void createRequest(String phone, Context context, RequestGenerator.OnRequestCompleteListener listener) {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("phone", phone));

        RequestGenerator generator = new RequestGenerator(context);
        generator.setOnRequestCompletteListener(listener);
        generator.execute("/api/auth/phone", params, PhoneResponse.class);
    }
    public static void writeToken(String token, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Main.SETTINGS_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(Main.TOKEN, token).commit();
        Log.d("TEST", "token= " + token + "was writing");
    }
}
