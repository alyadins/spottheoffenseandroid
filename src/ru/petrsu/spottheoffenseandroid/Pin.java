package ru.petrsu.spottheoffenseandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import ru.petrsu.spottheoffenseandroid.network.RequestGenerator;
import ru.petrsu.spottheoffenseandroid.network.Response.PhoneResponse;
import ru.petrsu.spottheoffenseandroid.network.Response.Response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lexer on 04.06.14.
 */
public class Pin extends Activity implements View.OnClickListener, RequestGenerator.OnRequestCompleteListener{

    private EditText pinTextView;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin);

        pinTextView = (EditText) findViewById(R.id.pin);
        findViewById(R.id.pin_repeat).setOnClickListener(this);
        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.wait_message));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.ok_action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pin_repeat:
                String phone = getSharedPreferences(Main.SETTINGS_NAME, Context.MODE_PRIVATE).getString(Main.PHONE, "0");
                if (!phone.equals("0"))
                    Phone.createRequest(phone, Pin.this, new RequestGenerator.OnRequestCompleteListener() {
                        @Override
                        public <T> void onComplete(T object) {
                            PhoneResponse response = (PhoneResponse) object;
                            Phone.writeToken(response.token, Pin.this);
                        }
                    });
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.continue_action:
                createRequest();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void createRequest() {
        SharedPreferences preferences = getSharedPreferences(Main.SETTINGS_NAME, Context.MODE_PRIVATE);

        String token = preferences.getString(Main.TOKEN, "0");

        if (token.equals("0")) {
            startActivity(new Intent(Pin.this, Phone.class));
            finish();
        }

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("pin", pinTextView.getText().toString()));
        params.add(new BasicNameValuePair("token", token));

        RequestGenerator generator = new RequestGenerator(Pin.this);
        generator.setOnRequestCompletteListener(this);
        dialog.show();
        generator.execute("/api/auth/pin", params, Response.class);
    }
    @Override
    public <T> void onComplete(T object) {
        Response r = (Response) object;

        dialog.dismiss();

        if (r.code.equals("404")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(Pin.this);
            builder.setMessage(Pin.this.getString(R.string.pin_error))
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builder.create().show();
        } else if (r.code.equals("200")) {
            startActivity(new Intent(Pin.this, OffenseCreate.class));
            finish();
        }
    }
}
