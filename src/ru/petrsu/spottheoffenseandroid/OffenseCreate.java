package ru.petrsu.spottheoffenseandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import ru.petrsu.spottheoffenseandroid.UI.CarNumber;
import ru.petrsu.spottheoffenseandroid.UI.CustomTable;
import ru.petrsu.spottheoffenseandroid.UI.DeletableImageView;
import ru.petrsu.spottheoffenseandroid.UI.NumberDialog;
import ru.petrsu.spottheoffenseandroid.network.RequestGenerator;
import ru.petrsu.spottheoffenseandroid.network.Response.Response;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by lexer on 04.06.14.
 */
public class OffenseCreate extends Activity implements View.OnClickListener, DeletableImageView.OnDeleteListener, LocationListener{

    private static final int IMAGE_REQUEST_CODE = 1;
    public static final int OFFENSE_REQUEST_CODE = 2;

    private CustomTable table;
    private DeletableImageView imageView;
    private ImageView addButton;
    private TextView offenseTitle;
    private EditText comment;

    private CarNumber carNumber;
    private List<View> views;

    private String carNumberString = "A001AA01";
    private int id;
    private double[] location = new double[2];
    private List<Bitmap> images = new ArrayList<Bitmap>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offense_create);

        offenseTitle = (TextView) findViewById(R.id.offense_title);
        comment = (EditText) findViewById(R.id.comment);

        views = new ArrayList<View>();

        table = (CustomTable) findViewById(R.id.image_table);
        addButton = (ImageView) getLayoutInflater().inflate(R.layout.add_button, null);
        addButton.setOnClickListener(this);

        views.add(addButton);

        table.setViews(views);

        table.setOnSizeChangedListener(new CustomTable.OnTableSizeChanged() {
            @Override
            public void onSizeChanged(CustomTable table) {
                table.updateViews();
            }
        });

        carNumber = (CarNumber) findViewById(R.id.car_number);
        carNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberDialog dialog = new NumberDialog(OffenseCreate.this);

                dialog.setOkListener(new NumberDialog.OkListener() {
                    @Override
                    public void onOk(NumberDialog dialog, String result) {
                        carNumberString = result;
                        carNumber.setNumber(carNumberString);
                    }
                });
                dialog.show();
            }
        });

        findViewById(R.id.chose_offense).setOnClickListener(this);

        table.requestFocus();

        getCoordinates();

        location[0] = 0.0;
        location[1] = 0.0;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_button:
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, IMAGE_REQUEST_CODE);
                }
                break;
            case R.id.chose_offense:
                Intent intent = new Intent(OffenseCreate.this, OffenseTypes.class);
                startActivityForResult(intent, OFFENSE_REQUEST_CODE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bundle extras = data.getExtras();
        if (requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            images.add(imageBitmap);
            imageView = new DeletableImageView(OffenseCreate.this);
            imageView.setImage(imageBitmap);

            imageView.setTag(table.getNumberOfChild() + 1);
            imageView.setOnDeleteListener(this);

            views.remove(addButton);
            views.add(imageView);
            views.add(addButton);

            table.updateViews();
        } else if (requestCode == OFFENSE_REQUEST_CODE && resultCode == RESULT_OK) {
            id = extras.getInt("id");
            String title = extras.getString("title");

            offenseTitle.setText(title);
        }
    }

    public void getCoordinates() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, false);

        locationManager.requestLocationUpdates(provider, 0, 0, this);
    }

    @Override
    public void onDelete(DeletableImageView view) {
        images.remove(view.getBitmap());
        views.remove(view);
        table.updateViews();
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            this.location[0] = location.getLatitude();
            this.location[1] = location.getLongitude();
        } catch (NullPointerException e) {
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.create_offense, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.send_action:
                if (location[0] == 0.0 || location[1] == 0.0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(OffenseCreate.this);
                    builder.setMessage(R.string.coordinate_error)
                            .setPositiveButton(R.string.yes, coordinatesErrorListener)
                            .setNegativeButton(R.string.no, coordinatesErrorListener);
                    builder.create().show();
                } else {
                    sendData();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private DialogInterface.OnClickListener coordinatesErrorListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    sendData();
                    break;
            }
        }
    };

    private void sendData() {
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        String date = getDate();
        String token = getSharedPreferences(Main.SETTINGS_NAME, Context.MODE_PRIVATE).getString(Main.TOKEN, "0");
        String carNumber = carNumberString;
        String offenseType = String.valueOf(id);
        String latitude = String.valueOf(location[0]);
        String longitude = String.valueOf(location[1]);
        String comment = "";
        try {
            comment = URLEncoder.encode(this.comment.getText().toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        List<String> images64 = new ArrayList<String>();
        for (int i = 0; i < images.size(); i++) {
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            images.get(i).compress(Bitmap.CompressFormat.PNG, 100, bao);
            byte [] ba = bao.toByteArray();

            String image64= Base64.encodeToString(ba, Base64.DEFAULT);

            images64.add(image64);
        }

        Log.d("TEST", comment);
        params.add(new BasicNameValuePair("time", date));
        params.add(new BasicNameValuePair("token", token));
        params.add(new BasicNameValuePair("car_number", carNumber));
        params.add(new BasicNameValuePair("offense_type", offenseType));
        params.add(new BasicNameValuePair("latitude", latitude));
        params.add(new BasicNameValuePair("longitude", longitude));
        params.add(new BasicNameValuePair("comment", comment));

        for (int i = 0; i < images.size(); i++) {
            params.add(new BasicNameValuePair("image[]", images64.get(i)));
        }

        RequestGenerator generator = new RequestGenerator(OffenseCreate.this);
        generator.setOnRequestCompletteListener(responseListener);
        generator.execute("/api/offense/post", params, Response.class);
    }

    RequestGenerator.OnRequestCompleteListener responseListener = new RequestGenerator.OnRequestCompleteListener() {
        @Override
        public <T> void onComplete(T object) {
            Response r = (Response) object;
//            Log.d("TEST", r.code);
        }
    };

    private String getDate() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ZZZZ");
        return format.format(Calendar.getInstance().getTime());
    }
}
