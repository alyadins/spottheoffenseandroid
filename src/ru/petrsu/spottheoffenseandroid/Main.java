package ru.petrsu.spottheoffenseandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class Main extends Activity {
    /**
     * Called when the activity is first created.
     */

    public static final String SETTINGS_NAME = "spottheoffensesettings";
    public static final String TOKEN = "token";
    public static final String PHONE = "phone";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        startActivity(new Intent(Main.this, OffenseCreate.class));
        finish();
    }
}
