package ru.petrsu.spottheoffenseandroid.network;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;

/**
 * Created by lexer on 04.06.14.
 */
public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

    private OnCompleteListener listener;

    protected Bitmap doInBackground(String... urls) {
        String url = urls[0];
        Bitmap dBitmap = null;
        try {
            InputStream in = new java.net.URL(url).openStream();
            dBitmap = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dBitmap;
    }

    protected void onPostExecute(Bitmap result) {
        if (listener != null) {
            listener.onComplete(result);
        }
    }

    public void setOnCompleteListener(OnCompleteListener l) {
        this.listener = l;
    }

    public interface OnCompleteListener{
        public void onComplete(Bitmap bitmap);
    }
}
