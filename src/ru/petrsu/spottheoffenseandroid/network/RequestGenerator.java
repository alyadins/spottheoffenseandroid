package ru.petrsu.spottheoffenseandroid.network;

import android.content.Context;
import com.google.gson.Gson;
import org.apache.http.NameValuePair;

import java.util.List;

/**
 * Created by lexer on 02.06.14.
 */
public class RequestGenerator<T> implements AsyncRequest.OnRequestCompleteListener{

    private static final String URL = "http://spottheoffense.tk";
    private Class<? extends Object> serializable;

    private OnRequestCompleteListener listener;

    private Context context;

    private AsyncRequest.Type type = AsyncRequest.Type.POST;

    public RequestGenerator(Context context) {
        this.context = context;
    }

    public void execute(String relativeUrl, List<NameValuePair> params, Class<? extends Object> serializable) {
        this.serializable = serializable;

        AsyncRequest myAsyncRequest = new AsyncRequest(URL + relativeUrl, type);
        myAsyncRequest.setOnRequestCompleteListener(this);
        myAsyncRequest.execute(params);
    }

    @Override
    public void onComplete(String response) {

        Gson gson = new Gson();
        if (listener != null) {
            listener.onComplete(gson.fromJson(response, serializable));
        }
    }

    public void setOnRequestCompletteListener(OnRequestCompleteListener l) {
        this.listener = l;
    }

    public interface OnRequestCompleteListener {
        public <T> void onComplete(T object);
    }

    public void setType(AsyncRequest.Type type) {
        this.type = type;
    }
}
