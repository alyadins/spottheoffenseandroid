package ru.petrsu.spottheoffenseandroid.network;

import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpProtocolParams;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by lexer on 02.06.14.
 */
public class AsyncRequest extends AsyncTask<List<NameValuePair>, Integer, String> {

    public enum Type {
        POST,
        GET
    }

    private String url;
    private DefaultHttpClient httpClient;
    private StringBuffer sb;

    private OnRequestCompleteListener listener;

    private Type type = Type.POST;

    public AsyncRequest(String url) {
        this.url = url;
        httpClient = CustomHttpClient.getHttpClient();
    }

    public AsyncRequest(String url, Type type) {
        this(url);
        this.type = type;
    }

    public AsyncRequest(String url, OnRequestCompleteListener listener) {
        this.listener = listener;
        this.url = url;
    }

    @Override
    protected String doInBackground(List<NameValuePair>... lists) {

        sb =  new StringBuffer();
        BufferedReader in = null;

        try {
            HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), false); //making 3G network works*

            HttpResponse response;
            switch (type) {
                case GET:
                    HttpGet getRequest = new HttpGet(addParamsToUrl(url, lists[0]));
                    response = httpClient.execute(getRequest);
                    break;
                default:
                    HttpPost postRequest = new HttpPost(url);
                    UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(lists[0]);
                    postRequest.setEntity(formEntity);
                    response = httpClient.execute(postRequest);
                    break;
            }

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }
            in.close();

            Log.d("TEST", sb.toString());
        }
        catch(Exception ex)
        {
            ex.printStackTrace();

        }

        return sb.toString();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if (listener != null) {
            listener.onComplete(s);
        }
    }


    private String addParamsToUrl(String url, List<NameValuePair> params) {
        if (!url.endsWith("?")) {
            url += "?";
        }

        String paramString = URLEncodedUtils.format(params, "utf-8");

        url += paramString;

        return url;
    }

    public void setOnRequestCompleteListener(OnRequestCompleteListener l) {
        listener = l;
    }

    public interface OnRequestCompleteListener {
        public void onComplete(String response);
    }
}
