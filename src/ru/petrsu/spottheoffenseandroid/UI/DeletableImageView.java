package ru.petrsu.spottheoffenseandroid.UI;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import ru.petrsu.spottheoffenseandroid.R;

/**
 * Created by lexer on 04.06.14.
 */
public class DeletableImageView extends RelativeLayout implements View.OnClickListener {

    private OnDeleteListener deleteListener;
    private View deleteButton;
    private ImageView image;

    public DeletableImageView(Context context) {
        super(context);
        init();
    }

    public DeletableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DeletableImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.deleteable_image_view, this);

        deleteButton = findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(this);

        image = (ImageView) findViewById(R.id.image);
    }

    public void setOnDeleteListener(OnDeleteListener l) {
        this.deleteListener = l;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.delete_button:
                if (deleteListener != null) {
                    deleteListener.onDelete(this);
                }
                break;
        }
    }

    public void setImage(Bitmap bitmap) {
        image.setImageBitmap(bitmap);
    }

    public Bitmap getBitmap() {
        return ((BitmapDrawable) image.getDrawable()).getBitmap();
    }

    public interface OnDeleteListener {
        public void onDelete(DeletableImageView view);
    }
}