package ru.petrsu.spottheoffenseandroid.UI;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import ru.petrsu.spottheoffenseandroid.R;
import ru.petrsu.spottheoffenseandroid.utils.Font;

/**
 * Created by lexer on 04.06.14.
 */
public class CarNumber extends LinearLayout {

    private TextView numberTextView;
    private TextView regionTextView;

    public CarNumber(Context context) {
        super(context);
        init();
    }

    public CarNumber(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CarNumber(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.car_number, this);

        numberTextView = (TextView) findViewById(R.id.number);
        regionTextView = (TextView) findViewById(R.id.region);

        Font.instance.setTypeface(numberTextView, "number_font");
        Font.instance.setTypeface(regionTextView, "number_font");
    }

    public void setNumber(String number) {
        numberTextView.setText(number.substring(0, 6));
        regionTextView.setText(number.substring(6, number.length()));
    }
}
