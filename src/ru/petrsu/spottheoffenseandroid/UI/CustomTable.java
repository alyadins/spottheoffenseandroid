package ru.petrsu.spottheoffenseandroid.UI;

import android.animation.LayoutTransition;
import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

/**
 * Created by lexer on 04.06.14.
 */
public class CustomTable extends LinearLayout {


    public static final int CAPACITY = 4;
    private LinearLayout topRow;
    private LinearLayout bottomRow;

    private OnTableSizeChanged sizeListener;
    private View addButton;

    private List<View> views;

    public CustomTable(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOrientation(VERTICAL);

        topRow = new LinearLayout(getContext());
        bottomRow = new LinearLayout(getContext());

        topRow.setOrientation(HORIZONTAL);
        bottomRow.setOrientation(HORIZONTAL);

        topRow.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));
        bottomRow.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));

        LayoutTransition transition = new LayoutTransition();
        topRow.setLayoutTransition(transition);
        bottomRow.setLayoutTransition(transition);

        transition.setStagger(LayoutTransition.CHANGE_APPEARING, 30);
        transition.setStagger(LayoutTransition.CHANGE_DISAPPEARING, 30);

        topRow.setGravity(Gravity.CENTER_HORIZONTAL);
        bottomRow.setGravity(Gravity.CENTER_HORIZONTAL);

        addView(topRow);
        addView(bottomRow);
    }

    public int getNumberOfChild() {
        return topRow.getChildCount() + bottomRow.getChildCount();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (w == 0 || h == 0) {
            return;
        } else if (sizeListener != null) {
            sizeListener.onSizeChanged(this);
        }
    }


    public void setOnSizeChangedListener(OnTableSizeChanged l) {
        this.sizeListener = l;
    }

    public void setViews(List<View> views) {
        this.views = views;
    }

    public void updateViews() {
        if (views == null || views.size() == 0) {
            return;
        }

        topRow.removeAllViews();
        bottomRow.removeAllViews();


        for (int i = 0; i < views.size(); i++) {
            View v  = views.get(i);

            int side = Math.min(getWidth(), getHeight()) / 2;
            v.setLayoutParams(new LayoutParams(side, side));
            if (i < 2) {
                topRow.addView(v);
            } else if (i < 4) {
                bottomRow.addView(v);
            }
        }
    }

    public interface OnTableSizeChanged {
        public void onSizeChanged(CustomTable table);
    }
}