package ru.petrsu.spottheoffenseandroid.UI;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import ru.petrsu.spottheoffenseandroid.R;

/**
 * Created by lexer on 05.06.14.
 */
public class NumberDialog extends AlertDialog {

    private NumberPicker firstNumber;
    private NumberPicker firstLetter;
    private NumberPicker secondLetter;
    private NumberPicker thirdLetter;
    private NumberPicker secondNumber;
    private NumberPicker thirdNumber;
    private EditText regionEdit;


    private String[] lettersRus = {"А", "В", "Е", "К", "М", "Н", "О", "Р", "С", "Т", "У", "Х"};
    private String[] letters = {"A", "B", "E", "K", "M", "H", "O", "P", "C", "T", "Y", "X"};

    private OkListener okListener;

    public NumberDialog(Context context) {
        super(context);
        init();
    }

    public NumberDialog(Context context, int theme) {
        super(context, theme);
        init();
    }

    protected NumberDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Service.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.car_number_picker, null);

        setView(v);

        setButton(DialogInterface.BUTTON_POSITIVE, getContext().getString(R.string.ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (okListener != null) {
                    okListener.onOk(NumberDialog.this, getValue());
                }
            }
        });

        setTitle(getContext().getString(R.string.car_number_picker_title));

        firstNumber = (NumberPicker) v.findViewById(R.id.first_number);
        firstLetter = (NumberPicker) v.findViewById(R.id.first_letter);
        secondLetter = (NumberPicker) v.findViewById(R.id.second_letter);
        thirdLetter = (NumberPicker) v.findViewById(R.id.third_letter);
        secondNumber = (NumberPicker) v.findViewById(R.id.second_number);
        thirdNumber = (NumberPicker) v.findViewById(R.id.third_number);
        regionEdit = (EditText) v.findViewById(R.id.region);

        firstNumber.setMinValue(0);
        firstNumber.setMaxValue(9);

        secondNumber.setMinValue(0);
        secondNumber.setMaxValue(9);

        thirdNumber.setMinValue(0);
        thirdNumber.setMaxValue(9);

        int n = lettersRus.length;

        firstLetter.setDisplayedValues(lettersRus);
        firstLetter.setMinValue(0);
        firstLetter.setMaxValue(n - 1);

        secondLetter.setDisplayedValues(lettersRus);
        secondLetter.setMinValue(0);
        secondLetter.setMaxValue(n - 1);

        thirdLetter.setDisplayedValues(lettersRus);
        thirdLetter.setMinValue(0);
        thirdLetter.setMaxValue(n - 1);
    }

    public String getValue() {
        String result = "";
        result += letters[firstLetter.getValue()];
        result += firstNumber.getValue() + firstNumber.getMinValue();
        result += secondNumber.getValue();
        result += thirdNumber.getValue();
        result += letters[secondLetter.getValue()];
        result += letters[thirdLetter.getValue()];
        result += regionEdit.getText().toString();

        return  result;
    }

    public void setOkListener(OkListener l) {
        this.okListener = l;
    }

    public interface OkListener {
        public void onOk(NumberDialog dialog, String result);
    }
}
