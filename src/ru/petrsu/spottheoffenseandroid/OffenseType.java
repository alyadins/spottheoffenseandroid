package ru.petrsu.spottheoffenseandroid;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import ru.petrsu.spottheoffenseandroid.network.AsyncRequest;
import ru.petrsu.spottheoffenseandroid.network.ImageDownloader;
import ru.petrsu.spottheoffenseandroid.network.RequestGenerator;
import ru.petrsu.spottheoffenseandroid.network.Response.OffenseTypeBig;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lexer on 04.06.14.
 */
public class OffenseType extends Activity implements RequestGenerator.OnRequestCompleteListener, ImageDownloader.OnCompleteListener {

    private int id;
    private String title;

    private OffenseTypeBig offense;

    private ImageView image;
    private TextView titleTextView;
    private TextView descriptionTextView;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offense_type);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle b = getIntent().getExtras();
        id = b.getInt("id", 0);
        title = b.getString("title", "");

        image = (ImageView) findViewById(R.id.image);
        titleTextView = (TextView) findViewById(R.id.title);
        descriptionTextView = (TextView) findViewById(R.id.description);
        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.wait_message));

        titleTextView.setText(title);

        createRequest();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void createRequest() {
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair("id", String.valueOf(id)));

        RequestGenerator generator = new RequestGenerator(OffenseType.this);
        generator.setOnRequestCompletteListener(this);
        generator.setType(AsyncRequest.Type.GET);
        dialog.show();
        generator.execute("/api/offense_type/get", params, OffenseTypeBig.class);
    }

    @Override
    public <T> void onComplete(T object) {
        offense = (OffenseTypeBig) object;

        descriptionTextView.setText(offense.description);
        ImageDownloader downloader = new ImageDownloader();
        downloader.setOnCompleteListener(this);

        downloader.execute(offense.big_image);
    }

    @Override
    public void onComplete(Bitmap bitmap) {
        image.setImageBitmap(bitmap);
        dialog.dismiss();
    }
}
