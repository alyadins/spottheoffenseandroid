package ru.petrsu.spottheoffenseandroid.utils;

import android.graphics.Bitmap;

/**
 * Created by lexer on 04.06.14.
 */
public class RowItem {
    public int id;
    public String title;
    public Bitmap image;

    public RowItem(int id, String title, Bitmap image) {
        this.id = id;
        this.title = title;
        this.image = image;
    }

    public RowItem(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public void setBitmap(Bitmap bitmap) {
        this.image = bitmap;
    }
}