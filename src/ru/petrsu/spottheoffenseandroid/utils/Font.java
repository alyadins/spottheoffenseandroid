package ru.petrsu.spottheoffenseandroid.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Font.java
 * Author: eluiandil
 */
public class Font {

    public static final Font instance = new Font();

    private Font() { }

    public void setTypeface(TextView view, String font) {

        if(view == null)
            return;


        Typeface  typeface = getTypeFace(font, view.getContext());

        if(typeface == null)
            return;

        view.setTypeface(typeface);
    }

    public Typeface getTypeFace(String font, Context context) {
        return Typeface.createFromAsset(context.getAssets(), "font/" + font + ".ttf");
    }
}
