package ru.petrsu.spottheoffenseandroid;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import org.apache.http.NameValuePair;
import ru.petrsu.spottheoffenseandroid.network.AsyncRequest;
import ru.petrsu.spottheoffenseandroid.network.ImageDownloader;
import ru.petrsu.spottheoffenseandroid.network.RequestGenerator;
import ru.petrsu.spottheoffenseandroid.network.Response.OffenseTypeSmall;
import ru.petrsu.spottheoffenseandroid.utils.RowItem;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by lexer on 04.06.14.
 */
public class OffenseTypes extends Activity implements RequestGenerator.OnRequestCompleteListener {

    private OffenseTypeSmall[] offenseTypes;

    private ListView listView;

    private List<RowItem> items;

    private int counter;

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offense_types);

        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.wait_message));
        createRequest();
    }

    private void createRequest() {
        List<NameValuePair> params = new ArrayList<NameValuePair>();

        RequestGenerator generator = new RequestGenerator(OffenseTypes.this);
        generator.setOnRequestCompletteListener(this);
        generator.setType(AsyncRequest.Type.GET);
        dialog.show();
        generator.execute("/api/offense_type/all", params, OffenseTypeSmall[].class);
    }

    @Override
    public <T> void onComplete(T object) {
        offenseTypes = (OffenseTypeSmall[]) object;

        listView = (ListView) findViewById(R.id.offense_list);

        items = new ArrayList<RowItem>();

        for (OffenseTypeSmall ot : offenseTypes) {
            items.add(new RowItem(ot.id, ot.title));
        }

        loadImages();
    }

    private void loadImages() {
        ImageDownloader downloader = new ImageDownloader();
        counter = 0;
        downloader.setOnCompleteListener(onCompleteListener);
        if (offenseTypes.length != 0)
            downloader.execute(offenseTypes[0].small_image);
    }

    private ImageDownloader.OnCompleteListener onCompleteListener = new ImageDownloader.OnCompleteListener() {
        @Override
        public void onComplete(Bitmap bitmap) {
            int n = offenseTypes.length;
            items.get(counter).image = bitmap;
            incrementCounter();
            if (counter == n) {
                OffensesArrayAdapter adapter = new OffensesArrayAdapter(OffenseTypes.this, R.layout.offense_type_row, items, onItemClickListener, onInfoClockListener);
                listView.setAdapter(adapter);
                dialog.dismiss();
            } else {
                ImageDownloader downloader = new ImageDownloader();
                downloader.setOnCompleteListener(onCompleteListener);
                downloader.execute(offenseTypes[counter].small_image);
            }
        }
    };

    public void incrementCounter() {
        counter++;
    }

    private View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = (Integer) v.getTag();
            Intent intent = new Intent();
            intent.putExtra("id", id);
            intent.putExtra("title", getOffenseTitleById(id));

            setResult(RESULT_OK, intent);
            finish();
        }
    };

    private View.OnClickListener onInfoClockListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Integer i = (Integer) v.getTag();

            Intent intent = new Intent(OffenseTypes.this, OffenseType.class);
            Bundle b = new Bundle();
            b.putInt("id", i);
            b.putString("title", getTitleById(i));
            intent.putExtras(b);
            startActivity(intent);
        }
    };

    private String getTitleById(int id) {
        for (int i = 0; i < offenseTypes.length; i++) {
            if (offenseTypes[i].id == id) {
                return offenseTypes[i].title;
            }
        }
        return "";
    }


    private String getOffenseTitleById(int id) {
        for (int i = 0; i < offenseTypes.length; i++) {
            if (offenseTypes[i].id == id) {
                return offenseTypes[i].title;
            }
        }

        return "";
    }
    class OffensesArrayAdapter extends ArrayAdapter<RowItem> {


        private View.OnClickListener onItemClickListener;
        private View.OnClickListener onInfoClickListener;

        public OffensesArrayAdapter(Context context, int resource, List<RowItem> objects, View.OnClickListener onItemClickListener,
                                    View.OnClickListener onInfoClickListener) {
            super(context, resource, objects);

            this.onInfoClickListener = onInfoClickListener;
            this.onItemClickListener = onItemClickListener;
        }

        private class ViewHolder {
            ImageView imageView;
            TextView textView;
            ImageView info;
            View itemLayout;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            RowItem rowItem = getItem(position);

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.offense_type_row, null);
                holder = new ViewHolder();
                holder.textView = (TextView) convertView.findViewById(R.id.text);
                holder.imageView = (ImageView) convertView.findViewById(R.id.image);
                holder.info = (ImageView) convertView.findViewById(R.id.info);
                holder.itemLayout = convertView.findViewById(R.id.item_layout);

                holder.info.setOnClickListener(onInfoClickListener);
                holder.itemLayout.setOnClickListener(onItemClickListener);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.textView.setText(rowItem.title);
            holder.imageView.setImageBitmap(rowItem.image);
            holder.info.setTag(rowItem.id);
            holder.itemLayout.setTag(rowItem.id);

            return convertView;
        }
    }
}
